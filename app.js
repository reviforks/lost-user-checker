const config = require('./config.json');
const requirements = require('./requirements.js');
const Discord = require('discord.js');
const client = new Discord.Client({ partials: ['MESSAGE', 'CHANNEL', 'REACTION'] });
const cmd = require('node-cmd');
const git = require('simple-git/promise');
const got = require('got');
const htmlent  = require('html-entities').AllHtmlEntities;
const util = require('util');
const cheerio = require('cheerio');
const stringSimilarity = require('string-similarity');

const moderatorRoles = ['Moderator','Wiki Team','Staff'];
const disallowedChars = new RegExp('[\|\&\*\+\<\>]');
const iterativeCaseCheckLimit = 10;
const channels = {
	'bot_commands': '572854308694130698',
	'general': '563022855516717071',
	'mod_chat': '563038390963142656',
	'pcj_general': '596393599302893571',
	'verification': '563023179337957376',
	'moderation_log': '572823943124090953',
	'i18n': '576026924103630859',
	'grasp_rc': '565974923814895618',
	'rename_log': '604458699905564701',
	'verification_log': '596075927063232554',
	'roles': '620727832972099611',
	'channel_moves': '642562122525704202',
	'rules': '563032662093201464',
	'random': '563024520101888010'
};
const approvedChannels = [channels.verification, channels.bot_commands, channels.pcj_general, channels.mod_chat, channels.grasp_rc, channels.roles];
const genderRoles = [{"name":"He/Him","codes":["he","him"]},{"name":"She/Her","codes":["she","her"]},{"name":"They/Them","codes":["they","them"]},{"name":"Other","codes":["other"]}];
const eGenderRoles = [{"name":"He/Him","emoji":"💙"},{"name":"She/Her","emoji":"❤"},{"name":"They/Them","emoji":"💕"},{"name":"Other","emoji":"💛"}];
const languageRoles = [{"name":"Chinese","codes":["zh"]},{"name":"Dutch","codes":["nl"]},{"name":"Finnish","codes":["fi"]},{"name":"French","codes":["fr"]},{"name":"German","codes":["de"]},{"name":"Indonesian","codes":["in","id"]},{"name":"Italian","codes":["it"]},{"name":"Japanese","codes":["ja"]},{"name":"Korean","codes":["kr"]},{"name":"Norwegian","codes":["no"]},{"name":"Polish","codes":["pl"]},{"name":"Portuguese/Brazilian","codes":["pt","ptbr","pt-br"]},{"name":"Russian","codes":["ru"]},{"name":"Spanish","codes":["es"]},{"name":"Swedish","codes":["sv"]},{"name":"Thai","codes":["th"]},{"name":"Turkish","codes":["tr"]},{"name":"Ukrainian","codes":["uk"]},{"name":"Vietnamese","codes":["vi"]}]
const eLanguageRoles = [{"name":"Chinese","emoji":"🇨🇳"},{"name":"Dutch","emoji":"🇳🇱"},{"name":"Finnish","emoji":"🇫🇮"},{"name":"French","emoji":"🇫🇷"},{"name":"German","emoji":"🇩🇪"},{"name":"Indonesian","emoji":"🇮🇩"},{"name":"Italian","emoji":"🇮🇹"},{"name":"Japanese","emoji":"🇯🇵"},{"name":"Korean","emoji":"🇰🇷"},{"name":"Norwegian","emoji":"🇳🇴"},{"name":"Polish","emoji":"🇵🇱"},{"name":"Portuguese/Brazilian","emoji":"🇵🇹"},{"name":"Russian","emoji":"🇷🇺"},{"name":"Spanish","emoji":"🇪🇸"},{"name":"Swedish","emoji":"🇸🇪"},{"name":"Thai","emoji":"🇹🇭"},{"name":"Turkish","emoji":"🇹🇷"},{"name":"Ukrainian","emoji":"🇺🇦"},{"name":"Vietnamese","emoji":"🇻🇳"}]
const eVerticalRoles = [{"name":"Gaming","emoji":"🎮"},{"name":"TV/Movies","emoji":"📺"},{"name":"Anime","emoji":"🍥"},{"name":"Books/Other","emoji":"📚"}];
const eTZRoles = [{"name":"Europe/Africa","emoji":"🌍"},{"name":"Asia/Oceania","emoji":"🌏"},{"name":"Americas","emoji":"🌎"}];
const ePRoles = [{"name":"Fandom","field":"<:fandom:572846818560442368>","emoji":"572846818560442368"},{"name":"Gamepedia","field":"<:gamepedia:591645534683398144>","emoji":"591645534683398144"}];
const abuseFilters = [
	{filter:/\b(n[1ie]gg[e32]r[sz]?|nagger|nigga)\b/i,action:"mute"},
	{filter:/^>>>$/,action:"delete"},
	{filter:/FUCK YOU ALL/,action:"mute"}
];

const roleReactionMessages = {
	'gender': '773217550233370635',
	'vertical': '773217744992338000',
	'language': '773217627086258231',
	'timezone': '773217787942404099',
	'platform': '773217520160866335'
};

var roleReactionMessageCache = [];

const services = {
	"fandom": {
		name: "Fandom",
		baseURL: 'https://community.fandom.com/',
		blockURL: 'https://community.fandom.com/Special:Contributions/%s?limit=1',
		discordURL: 'https://services.fandom.com/user-attribute/user/%d/attr/discordHandle?cb=%d',
		profileURL: 'https://community.fandom.com/Special:Contributions/%s?useskin=oasis',
		options: {}
	},
	"gamepedia": {
		name: "Gamepedia",
		baseURL: 'https://help.gamepedia.com/',
		blockURL: 'https://help.gamepedia.com/Special:GlobalBlockList?wpTarget=%s&limit=1',
		discordURL: 'https://help.gamepedia.com/api.php?action=profile&format=json&do=getPublicProfile&user_name=%s&cb=%d',
		profileURL: 'https://help.gamepedia.com/UserProfile:%s',		
		options: {}//{auth: config.tokens.hydra}
	}
};

const events = {
	MESSAGE_REACTION_ADD: 'messageReactionAdd',
	MESSAGE_REACTION_REMOVE: 'messageReactionRemove',
};

const cannedRandom = [
	`This conversation should be continued in <#${channels.random}>.`,
	`The moderators request the honor of this conversation's presence in <#${channels.random}>.`
];

var verifyRename = [];
var waitingForModerator = [];
var nameChangeReactions = [];
var nameChangeTimeouts = [];
var affectedChannels = [];
var alertQueuedForThisChannelMove = false;

capitalize = (s) => {
  if (typeof s !== 'string') return '';
  return s.charAt(0).toUpperCase() + s.slice(1);
}

cleanTag = (tag) => {
	let split = tag.split('#');
	split[0] = split[0].trim();
	return split.join("#");
}

clearNameChangeTimeouts = () => {
	let tempArr = [];
	for (i of nameChangeTimeouts) {
		if (((new Date()).getTime() - i.time) < 172800000) tempArr.push(i);
		else clearTimeout(i.timeout);
	}
	nameChangeTimeouts = tempArr;
}

clearWaitingForModerator = () => {
	let tempArr = [];
	for (i of waitingForModerator) {
		if (((new Date()).getTime() - i.time) < 3600000) tempArr.push(i);
	}
	waitingForModerator = tempArr;
}

findGuildMember = async (guild, user) => {
	await guild.members.fetch();
	return guild.members.cache.find(m => m.user.id === user.id);
}

getAbbr = async (term) => {
	try {
		let res = await got('https://help.gamepedia.com/api.php',{
			searchParams: {
				action: 'cargoquery',
				tables: 'WikiAbbreviations',
				fields: 'WikiAbbreviations.Platform,WikiAbbreviations.Abbreviation,WikiAbbreviations.Full,WikiAbbreviations.Definition',
				where: `WikiAbbreviations.Abbreviation RLIKE "${term}"`,
				format: 'json'
			}
		});
		let abbrData = JSON.parse(res.body);
		return Promise.resolve(abbrData.cargoquery);
	}
	catch (error) {
		console.log(error);
		return Promise.resolve('unknown');
	}
}

getBlockStatus = async (username, platform) => {
	let retObject = {
			gblocked: false,
			disabled: false,
			blocked: false
	};
	try {
		let res = await got(util.format(services[platform].blockURL,wikiEncode(username)));
		let $ = cheerio.load(res.body);
		
		if (platform === "gamepedia") {
			retObject.gblocked = ($(".mw-blocklist bdi").eq(0).text() == username.replace(/_/g,' '));
		}
		else {
			const blockNotif = $(".mw-warning-with-logexcerpt");
			const disabledNotif = $("#mw-content-text .errorbox");

			retObject.disabled = disabledNotif.length > 0;

			if (blockNotif.length) {
				retObject.blocked = true;
				if (blockNotif.find(".mw-logline-block").length === 0) {
					retObject.gblocked = true;
					retObject.blocked = false;
				}
			}
		}
		return Promise.resolve(retObject);
	}
	catch (error) {
		return Promise.resolve(retObject);
	}
}

getDiscord = async (userData, service, channel) => {
	let profile = util.format(service.profileURL,wikiEncode(userData.name));
	try {
		let discordHandle = await getDiscordHandle({platforms:{fandom:{id:userData.id}},name:userData.name}, service);
		let profileLink = (discordHandle === "unknown") ? "" : `(<${profile}>) `;
		await channel.send(`${userData.name}'s ${profileLink}Discord tag on ${service.name} is ${discordHandle}`);	
	}
	catch (error) {
		if (error.response != undefined) {
			if (error.response.statusCode == 404) {
				await channel.send(`${userData.name}'s Discord tag on ${service.name} is unknown, but they do have an account there: <${profile}>`);
			}
		}
		else console.log(error);
	}
}

getDiscordHandle = async (userData, service) => {
	try {
		const response = await got(util.format(service.discordURL, (service===services.fandom?userData.platforms["fandom"].id:wikiEncode(userData.name)), Date.now()));
		const discordData = JSON.parse(response.body);
		let discordHandle = 'unknown';
		if (service === services.fandom) {
			discordHandle = cleanTag(new htmlent().decode(discordData.value));
		}
		else {
			if (discordData.errormsg != undefined || discordData.profile == undefined) {
				return Promise.resolve("unknown");
			}
			discordHandle = cleanTag(discordData.profile["link-discord"]);
		}
		let unknownUser = false;
		if (discordHandle.length > 37) return Promise.resolve("unknown");
		discordHandle = discordHandle.replace(/(<(@.*)>|@everyone)/g, "");
		if (discordHandle == "") return Promise.resolve("unknown");
		return Promise.resolve(discordHandle);
	}
	catch (error) {
		return Promise.resolve("unknown");
	}
}

getEditCount = async (username, platform) => {
	if (platform==="gamepedia") return Promise.resolve(0); // @TODO, maybe
	try {
		let res = await got(`https://community.fandom.com/Special:Editcount/${encodeURIComponent(user)}`);
    	let $ = cheerio.load(res.body);
    	let edits = $('.TablePager th').eq(7).text();
    	return Promise.resolve(parseInt(edits.replace(/,/g,''),10));
    }
    catch (error) {
    	return Promise.resolve(-1);
    }
}

getGroups = async (username, service) => {
	try {
		let output = await got(`${service.baseURL}api.php?action=query&format=json&list=users&usprop=registration|implicitgroups|groups&ususers=${wikiEncode(username)}`);
		let data = JSON.parse(output.body);
		if (data.error || !data.query.users[0] || typeof data.query.users[0].implicitgroups === "undefined" || data.query.users[0].missing != undefined) return Promise.resolve("user not found");
		return Promise.resolve(data.query.users[0]);
	}
	catch (error) {
		return Promise.resolve("user not found");
	}
}

processQuote = async (quote, msg) => {
	let embed = new Discord.MessageEmbed({
		author: {
			name: quote.author.tag,
			"icon_url": quote.author.avatarURL()
		},
		description: quote.content,
		timestamp: quote.createdTimestamp,
		fields: [
			{
				name: "Original post",
				value: `[Link](${quote.url})`
			}
		],
		footer: {
			text: `Quoted by ${msg.author.tag}`,
			"icon_url": msg.author.avatarURL()
		}
	});
	await msg.channel.send("",{embed:embed});
	await msg.delete();
}

removeNameChangeReactionMessage = async (msg) => {
	try {
		if (msg.embeds == undefined) return;
		if (msg.embeds.length === 0) return;
		let omDesc = msg.embeds[0].description.replace(/\nDisallow to prompt the user to revert the change \(and set a 24 hour timer to reset\)\.\nIgnore to do nothing/,"");
		await msg.edit(omDesc,{embed:null});
		await msg.reactions.removeAll();
		let index = nameChangeReactions.findIndex(m => m.id === msg.id);
		if (index === -1) return;
		nameChangeReactions.splice(index,1);
	}
	catch (error) {
		console.log(error);
	}
}

sleep = (ms) => {
	return new Promise(resolve => setTimeout(resolve, ms));
}

toggleRole = async (msg, role) => {
	if (role !== undefined) {
		try {
			if (msg.member.roles.cache.find( r => r.name == role.name) != undefined) {
				await msg.member.roles.remove(role, "By request");
				await msg.reply(`${role.name} role removed.`);
			}
			else {
				await msg.member.roles.add(role, "By request");
				await msg.reply(`${role.name} role added.`);
			}
		}
		catch (error) {
			console.log(error);
		}
	}
}

tryService = async (service, username, channel, results, reply) => {
	try {
		let response = await got(`https://community.fandom.com/api/v1/User/UsersByName?query=${wikiEncode(username)}&limit=50`);
	
		let userData = JSON.parse(response.body);
		for (user of userData.users) {
			if (user.name === username.replace(/_/g," ")) {
				getDiscord(user, service, channel);
				return;
			}
			results[service.name].push(user);
		}
		let possibleMatches = {};
		possibleMatches.fandom = [];
		for (u of results[services.fandom.name]) {
			if (u.name.replace(/ /g,"").localeCompare(username.replace(/ /g,""),"en",{sensitivity:"base"}) == 0 && possibleMatches.fandom.indexOf(u.name) == -1) possibleMatches.fandom.push(u.name);
		}

		let embed = {
  				color: 54741,
				description: `User not found. Here is the list of possible options:\n\n• [Users](https://community.fandom.com/api.php?action=query&list=allusers&aulimit=max&format=jsonfm&aufrom=${wikiEncode(username)})`,
				title: username,
				footer: {text: 'Verify User'},
				timestamp: new Date().toISOString()
			};

		if (possibleMatches.fandom.length) {
			embed.fields = [];
			let val = "";
			for (i in possibleMatches.fandom) {
				if (i>10) continue;
				if (i>0) val+="\n";
				val += `• <https://community.fandom.com/wiki/User:${wikiEncode(possibleMatches.fandom[i])}>`;
			}
			embed.fields.push({
				name: "Likely matches",
				value: val
			});
		}

		await reply.edit(reply.content,{embed: embed});
	}
	catch (error) {
		channel.send("Error retrieving results.");
		console.log(error);
	}
}

unmuteUser = async(member, msg, muteMatch) => {
	try {
		let role = msg.channel.guild.roles.cache.find(ro => ro.name === "Muted");
		await member.roles.remove(role,`Mute requested by ${msg.author} expired`);
		await msg.reply(`Muted restriction on <@${member.id}> has been lifted`);
		let channel = msg.guild.channels.cache.find(ch => ch.id === channels.moderation_log);
		let embed = new Discord.MessageEmbed({
			color: 12118406,
			title: member.user.tag,
			timestamp: msg.createdTimestamp,
			description: `Mute [requested by ${msg.author}](${msg.url}) for ${muteMatch[2]} ${muteMatch[3]} has now expired.`
		});
		await channel.send("",{embed:embed});
	}
	catch (error) { console.log(error); }
}

verifyUser = async (userData, msg) => {
	if (msg.member != null){
		if (msg.member.roles.cache.find(r => r.name == 'Verified') && !msg.member.roles.cache.find(r => moderatorRoles.indexOf(r.name) != -1)) {
			try {
				await msg.reply("you are already verified - there is no need to verify again.");
			}
			catch (error) {
				console.log(error);
			}
			return;
		}
	}
	try {
		let values = await Promise.all([
			getGroups(userData.name, services.fandom),
			getGroups(userData.name, services.gamepedia)
		]);
		grps = [{platform:"fandom", grps:values[0]}, {platform:"gamepedia", grps:values[1]}];
		for (g of grps) {
			if (g.grps == "user not found") continue;
			userData.platforms[g.platform].id = g.grps.userid;
			userData.platforms[g.platform].registration = g.grps.registration;
			userData.imgroups = userData.imgroups.concat(g.grps.implicitgroups);
			userData.groups = userData.groups.concat(g.grps.groups);
		}
		if (!Object.entries(userData.platforms).find(p => p[1].id != -1)) {
			let reply = await msg.reply("that username was not found. Please check to make sure you are using your wiki user name. Results from a more intensive search will be provided in a few moments.");
			let results = {
				Fandom: []
			};
			tryService(services.fandom, userData.name, msg.channel, results, reply);
			return;
		}
	}
	catch (error) {
		console.log(error);
		return;
	};

	try {
		let values = await Promise.all([
			getDiscordHandle(userData,services.fandom),getDiscordHandle(userData,services.gamepedia),
			getEditCount(userData.name,"fandom"),getEditCount(userData.name,"gamepedia"),
			getBlockStatus(userData.name,"fandom"),getBlockStatus(userData.name,"gamepedia")
		]);
		userData.platforms.fandom.discordHandle = values[0];
		userData.platforms.gamepedia.discordHandle = values[1];
		userData.platforms.fandom.edits = values[2];
		userData.platforms.gamepedia.edits = values[3];
		userData.platforms.fandom.blocks = values[4];
		userData.platforms.gamepedia.blocks = values[5];
		userData.platforms.fandom.requirementsPassed = {response:true};//requirements.check(userData, "fandom");
		userData.platforms.gamepedia.requirementsPassed = {response:true};//requirements.check(userData, "gamepedia");	
	}
	catch (error) {
		console.log(error);
		return;
	}

	let roles = [];
	let embed = new Discord.MessageEmbed({
		color: 65280,
		footer: { text: 'Verify User' },
		timestamp: new Date().toISOString(),
		title: userData.name,
		fields: []
	});

	oPlatforms = Object.entries(userData.platforms);
	if (p = oPlatforms.find(pl => pl[1].blocks.gblocked || pl[1].blocks.disabled)) {
		let profileURL = util.format(services[p[0]].profileURL,wikiEncode(userData.name));
		embed.setColor(16711680);
		embed.fields.push({name:'Profile',value:`[${userData.name}](${profileURL})`});
		embed.fields.push({name:'Tags', value:`${msg.author.toString()}`});
		embed.fields.push({name:'Account status',value:'REJECTED: \nCurrently globally blocked or disabled.'});
		try {			
			let reply = await msg.reply("",{embed:embed});
			embed.title = "Verification";
			embed.description = `**NOT APPROVED** ${msg.author.toString()}`;
			embed.fields = [
							{name:"Username",value:`\`${userData.name}\``},
							{name:"Link",value:`<${reply.url}>`},
							{name:"Reason",value:"Currently globally blocked or disabled."}
							];
			await msg.guild.channels.cache.get(channels.verification_log).send("",{embed:embed});
		}
		catch (error) { console.log(error); }
		return;
	}
	if (p = oPlatforms.find(pl => pl[1].blocks.blocked)) {
		let profileURL = util.format(services[p[0]].profileURL,wikiEncode(userData.name));
		embed.setColor(16776960);
		embed.fields.push({name:'Profile',value:`[${userData.name}](${profileURL})`});
		embed.fields.push({name:'Tags', value:`${msg.author.toString()}`});
		embed.fields.push({name:'Account status',value:'NOT APPROVED: \nActive block on Community Central.'});
		embed.fields.push({name:'Notice',value:'Please wait for a moderator\'s assistance.'});	
		waitingForModerator.push({id:msg.author.id,time:(new Date()).getTime()});
		try {			
			let reply = await msg.reply("",{embed:embed});
			embed.title = "Verification";
			embed.description = `**NOT APPROVED** ${msg.author.toString()}`;
			embed.fields = [
							{name:"Username",value:`\`${userData.name}\``},
							{name:"Link",value:`<${reply.url}>`},
							{name:"Reason",value:"Active block on Community Central."}
							];
			await msg.guild.channels.cache.get(channels.verification_log).send("",{embed:embed});
		}
		catch (error) { console.log(error); }
		return;
	}
	for (let k of oPlatforms.filter(pl => pl[1].id != -1)) {
		let p = k[0];
		let platform = userData.platforms[p]; 
		let profileURL = util.format(services[p].profileURL,wikiEncode(userData.name));
		if (!platform.requirementsPassed.response && roles.length === 0){// && !userData.platforms.gamepedia.requirementsPassed.response) {
			embed.setColor(16776960);
			embed.fields.push({name:capitalize(p),value:`${msg.author.toString()} - ${platform.discordHandle}`});
			embed.fields.push({name:'Profile',value:`[${userData.name}](${profileURL})`});
			embed.fields.push({name:'Account status',value:'NOT APPROVED: \nThe account DOES NOT MEET the requirements.'});
			embed.fields.push({name:'Notice',value:`Please **WAIT** for a moderator's assistance to help you verify.`});
			waitingForModerator.push({id:msg.author.id,time:(new Date()).getTime()});
			try {			
				let reply = await msg.reply("",{embed:embed});
				embed.title = "Verification";
				embed.description = `**NOT APPROVED** (Pending Moderator) - ${msg.author.toString()}`;
				embed.fields = [
								{name:"Username",value:`\`${userData.name}\``},
								{name:"Link",value:`<${reply.url}>`},
								{name:"Reason",value:`Requirements not met: ${platform.requirementsPassed.reasons.join(', ')}`}
								];
				await msg.guild.channels.cache.get(channels.mod_chat).send("",{embed:embed});
			}
			catch (error) { console.log(error); }
			return;
		}

		if (platform.discordHandle == msg.author.tag) {
			embed.fields.push({name:capitalize(p),value:`${msg.author.toString()} - ${platform.discordHandle}`});
			embed.fields.push({name:'Profile',value:`[${userData.name}](${profileURL})`});
			if (roles.length == 0) {
				let role = msg.guild.roles.cache.find(r => r.name === "Verified");
				roles.push(role);
			}
		}
	}
	
	if (roles.length > 0) {
		embed.fields.push({name:'Account status',value:`APPROVED: \nAccount status is valid.`});
		if (userData.groups.includes("council")) {
			role = msg.guild.roles.cache.find(r => r.name === "Community Council");
			roles.push(role);
		}
		if (userData.groups.includes("grasp")) {
			role = msg.guild.roles.cache.find(r => r.name === "GRASP");
			roles.push(role);
		}
		if (userData.groups.includes("vstf")) {
			role = msg.guild.roles.cache.find(r => r.name === "VSTF");
			roles.push(role);
		}
		roleText = "";
		for (r in roles) {
			roleText += `<@&${roles[r].id}>`;
			if (roles.length > 1 && r < (roles.length-1)) {
				if (roles.length > 2) roleText += ",";
				roleText += " ";
				if (roles.length > 1 && r == (roles.length-2)) roleText += "and ";
			}
		}
		roleText += (roles.length>1)?" roles":" role";
		let rolesCopy = roles;
		roles = [];
		for (r of rolesCopy) {
			if (!msg.member.roles.cache.find(ro => ro.id == r.id)) roles.push(r);
		}
		embed.fields.push({name:'Roles added',value:`You now have the ${roleText}.`});
		try {
			await msg.member.roles.add(roles, `Verified, added the ${roleText}`);
		}
		catch (error) { 
			if (error.message) {
				if (!error.message.includes("The set already contains this value")) console.log(error.message);
			}
			else console.log(error);
		}
		embed.fields.push({name:'Need more roles?',value:`Please visit <#${channels.roles}> to add other roles.`});
		if (userData.name.localeCompare(msg.author.username,"en",{sensitivity:"base"}) != 0) {
			try {
				verifyRename.push(userData.name.slice(0,31));
				await msg.member.setNickname(userData.name.slice(0,31),"Matching wiki name");
				let nick = '`/nick`';
				embed.fields.push({name:'Notice',value:`Your nickname has been automatically changed; you may change it via the ${nick} command. Please follow rule #5 in <#${channels.rules}>.`});
			}
			catch (error) {
				if (error.message) {
					if (error.message != 'Missing Permissions') console.log(error.message);
				}
				else console.log(error);
			}
		}
	}
	else {
		let discordHandle = userData.platforms.fandom.discordHandle;
		let p = 'fandom';
		if (discordHandle == undefined || discordHandle == '') {
			discordHandle = userData.platforms.gamepedia.discordHandle;
			p = 'gamepedia';
		}
		let profileURL = util.format(services.fandom.profileURL,wikiEncode(userData.name));
		embed.setColor(16776960);
		embed.fields.push({name:capitalize(p),value:`${msg.author.toString()} - ${discordHandle}`});
		embed.fields.push({name:'Profile',value:`[${userData.name}](${profileURL})`});
		embed.fields.push({name:'Account status',value:`NOT APPROVED: \nDiscord account DOES NOT MATCH ${capitalize(p)} Discord tag.`});
		embed.fields.push({name:'Notice',value:`Please add/correct your Discord tag to your profile and try again. Click on [this link](https://c.fandom.com/Special:VerifyUser/${encodeURIComponent(userData.name).replace(/([\(\)])/g,"\\$1")}?user=${encodeURIComponent(msg.author.username).replace(/([\(\)])/g,"\\$1")}&tag=${msg.author.discriminator}) for quick setup, or refer to this guide: [https://c.fandom.com/Help:Profiles/Discord](https://community.fandom.com/wiki/Help:Profiles/Discord)`});
	}
	try {			
		let reply = await msg.reply("",{embed:embed});
		if (roles.length == 0) return;
		embed.title = "Verification";
		embed.description = `**VERIFIED** ${msg.author.toString()}`;
		embed.fields = [
						{name:"Username",value:`\`${userData.name}\``},
						{name:"Link",value:`<${reply.url}>`},
						{name:"Groups",value:`<@&${roles.map(r => r.id).join(">, <@&")}>`}
						];
		await msg.guild.channels.cache.get(channels.verification_log).send("",{embed:embed});
	}
	catch (error) { 
		console.log(error);
	}
}

wikiEncode = (text) => {
	return encodeURIComponent(text.replace(/ /g,"_"));
}

client.once('ready', async () => {
	console.log(`Logged in as ${client.user.tag}!`);
    let guild = client.guilds.cache.get("563020189604773888");
	setInterval(clearWaitingForModerator, 3600000);
	setInterval(clearNameChangeTimeouts, 86400000);
	setTimeout(async function() {
		try { 
			let messages = await guild.channels.cache.get(channels.rename_log).messages.fetch({limit:30});
			messages.filter(msg => msg.author.id == client.user.id).every(async function() {
				await removeNameChangeReactionMessage(this);
			});
		}
		catch (error) { console.log(error); }
	}, 5000);
});

client.on('message', async initMsg => {
	let msg;
	if (initMsg.partial) {
		try {
			msg = await initMsg.fetch();
		}
		catch (error) {
			console.log(error);
			return;
		}
	}
	else msg = initMsg;
	if (msg.author.id === client.user.id) return;
	if (msg.channel.type === 'dm') {
		if (msg.author.username=="pcj") {
			await msg.reply(client.ws.ping);
			return;
		}
		await msg.reply(`If you are attempting to verify, you will need to do that in <#${channels.verification}>. See you there!`);
		return;
	}
	for (f of abuseFilters) {
		if (msg.content.match(f.filter)) {
			try {
				if (f.action == "delete") {
					await msg.delete();
					return;
				}
				if (!msg.member) {
					console.log(`${msg.author.tag}: ${msg.content} ${msg.url}`);
					return;
				}
				let role = msg.guild.roles.cache.find(ro => ro.name === "Muted");
				await msg.member.roles.add(role,"Automatically muted by abuse filter");
				await msg.reply("you have been automatically muted due to your message. Please stand by for moderator attention.");
				let channel = msg.guild.channels.cache.find(ch => ch.id === channels.moderation_log);
				let fString = '`'+f.filter.toString()+'`';
				let embed = new Discord.MessageEmbed({
					color: 14238017,
					title: "Automatic mute",
					timestamp: msg.createdTimestamp,
					description: msg.content,
					fields: [{name:"Reason",value:`<@${msg.member.id}> got muted for matching ${fString}\n\n[Source](${msg.url})`}]
				});
				await channel.send("",{embed:embed});
				channel = msg.guild.channels.cache.find(ch => ch.id === channels.mod_chat);
				await channel.send(`ATTENTION: User <@${msg.member.id}> has been automatically muted due to this comment: ${msg.url}`);
			}
			catch (error) { console.log(error); }
			return;
		}
	}
	if (msg.author.tag == "pcj#1150") {
		if (msg.content.toLowerCase().startsWith("!river")) {
			let terms = msg.content.slice(7).split(';');
			let embed = new Discord.MessageEmbed({
				title: "River's abbreviations, explained",
				color: 65280
			});
			for (term of terms) {
				let td = term.split(':');
				embed.addField(td[0],td[1],true);
			}
			await msg.channel.send("",{embed:embed});
			await msg.delete();
			return;
		}
	}
	if (msg.member) {
		if (msg.member.roles.cache.find(r => moderatorRoles.indexOf(r.name) != -1) || msg.channel.id === channels.pcj_general) {
			if (msg.content.match(/^<@.*?> mute$/i)) {
				try {
					let member = msg.mentions.members.first();
					if (member.roles.cache.find(r => r.name === "Muted")) {
						await msg.reply("the user is already muted.");
						return;
					}
					let role = msg.guild.roles.cache.find(ro => ro.name === "Muted");
					await member.roles.add(role, `muted by ${msg.author}`);
					await msg.reply(`User <@${member.id}> has been muted. The mute will not be automatically removed.`);
					let channel = msg.guild.channels.cache.find(ch => ch.id === channels.moderation_log);
					let embed = new Discord.MessageEmbed({
						color: 14238017,
						title: member.user.tag,
						timestamp: msg.createdTimestamp,
						description: `Muted [by request of ${msg.author}](${msg.url}) indefinitely.`
					});
					await channel.send("",{embed:embed});
				}
				catch (error) { console.log(error); }
				return;
			}
			if (avatarMatch = msg.content.match(/^!avatar (.*)/i)) {
				try {
					await msg.guild.members.fetch();
					let member = msg.guild.members.cache.find(m => m.user.tag === avatarMatch[1] || m.user.id == avatarMatch[1]);
					if (member == null) {
						if (msg.mentions.members.first()) {
							member = msg.mentions.members.first();
						}
						else {
							await msg.reply(`could not find user ${avatarMatch[1]}`);
							return;
						}
					}
					await msg.reply(`their avatar is <${member.user.avatarURL()}>`);
				}
				catch (error) {
					console.log(error);
				}
				return;
			}
			if (muteMatch = msg.content.match(/^!?um,? mute (.*) for ([\d]+) ?(s\w*|m\w*|h\w*).*$/i)) {
				try {
					await msg.guild.members.fetch();
					let member = msg.guild.members.cache.find(m => m.user.tag === muteMatch[1] || m.user.id == muteMatch[1]);
					if (member == null) {
						if (msg.mentions.members.first()) {
							member = msg.mentions.members.first();
						}
						else {
							await msg.reply(`could not find user ${muteMatch[1]}`);
							return;
						}
					}
					if (member.roles.cache.find(r => r.name === "Muted")) {
						await msg.reply("the user is already muted.");
						return;
					}
					let interval = 1;
					switch (muteMatch[3].toLowerCase().slice(0,1)) {
						case 'm':
							interval = 60;
							break;
						case 'h':
							interval = 3600;
							break;
						default:
							interval = 1;
					}
					let duration = parseInt(muteMatch[2]) * interval * 1000;
					let role = msg.guild.roles.cache.find(ro => ro.name === "Muted");
					await member.roles.add(role, `muted by ${msg.author} for ${muteMatch[2]} ${muteMatch[3]}`);
					setTimeout(unmuteUser, duration, member, msg, muteMatch);
					await msg.reply(`User <@${member.id}> has been muted. Mute should be lifted at ${new Date(Date.now()+duration).toUTCString()}`);
					let channel = msg.guild.channels.cache.find(ch => ch.id === channels.moderation_log);
					let embed = new Discord.MessageEmbed({
						color: 14238017,
						title: member.user.tag,
						timestamp: msg.createdTimestamp,
						description: `Muted [by request of ${msg.author}](${msg.url}) for ${muteMatch[2]} ${muteMatch[3]}\n\nShould expire at ${new Date(Date.now()+duration).toUTCString()}`
					});
					await channel.send("",{embed:embed});
				}
				catch (error) {console.log(error); }
				return;
			}
			if (msg.content.match(/^!?um,? update/i)) {
				try {
					await git().pull();
					await msg.reply("Updated. BRB");
					cmd.get('pm2 restart app');
				}
				catch (error) { console.log(error); }
				return;
			}
			if (msg.content.match(/^!?um,? reload/i)) {
				try {
					await msg.reply("OK, brb");
					cmd.get('pm2 restart app');
				}
				catch (error) { console.log(error); }
				return;
			}
			if (msg.content.toLowerCase() == "!random" || msg.content.trim() == `<#${channels.random}>`) {
				await msg.channel.send(cannedRandom[Math.floor(Math.random() * Math.floor(cannedRandom.length))]);
				await msg.delete();
				return;
			}
		}
	}
	if (quoteMatch = msg.content.match(/^https:\/\/(?:canary\.|ptb\.)?discord(?:app)?\.com\/channels\/(\d+)\/(\d+)\/(\d+)$/)) {
		try {
			if (msg.attachments.array().length > 0 || msg.embeds.length > 0) return;
			if (quoteMatch[1] != msg.guild.id) return;
			let channel = msg.guild.channels.cache.find(ch => ch.id == quoteMatch[2]);
			if (!channel) return;
			if (!channel.memberPermissions(msg.member).has('VIEW_CHANNEL')) return;
			let quote = await channel.messages.fetch(quoteMatch[3]);
			if (quote) await processQuote(quote, msg);
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.toLowerCase().startsWith('!ucp')) {
		await msg.channel.send(' • **Help** - <https://c.fandom.com/Help:UCP>.\n• **Information** - <https://fandom.zendesk.com/hc/articles/360044776693>.\n• **Bugs, features, changes** - <https://c.fandom.com/User:Noreplyz/UCP>.');
		return;
	}
	if (scMatch = msg.content.match(/^!sc( .*)?/)) {
		let embed = new Discord.MessageEmbed({
			color: 65280,
			title: 'Contact forms 																	:)'
		});
		embed.addField('Fandom',
			`[Knowledge base](https://fandom.zendesk.com/hc/en-us)
• [Account help](https://fandom.zendesk.com/hc/en-us/requests/new?ticket_form_id=360000931094)
• [Wiki changes](https://fandom.zendesk.com/hc/en-us/requests/new?ticket_form_id=360000931354)
• [Problems](https://fandom.zendesk.com/hc/en-us/requests/new?ticket_form_id=360000940393)
• [Protection](https://fandom.zendesk.com/hc/en-us/requests/new?ticket_form_id=360000948854)
• [Other](https://fandom.zendesk.com/hc/en-us/requests/new?ticket_form_id=360000956114)
Spam/Vandalism
• <#600818678682091562>
• [SOAP Wiki](https://soap.fandom.com)`, true);
		embed.addField('Gamepedia',
			`[Knowledge base](https://gamepedia.zendesk.com/hc/en-us)
• [Requests](https://gamepedia.zendesk.com/hc/en-us/requests/new?ticket_form_id=235968)
• [General issues](https://help.gamepedia.com/Reporting_issues_on_Gamepedia)
• [Bad ads](https://help.gamepedia.com/Reporting_bad_advertisements)
Spam/Vandalism
• <#573641974276423702>`, true);
		await msg.channel.send(embed);
		return;
	}
	if (abbrMatch = msg.content.match(/^!abbr( .+)?/i)) {
		userTerm = abbrMatch[1];
		if (userTerm === undefined) userTerm = '.*';
		userTerm = userTerm.trim();
		let abbrData = await getAbbr(userTerm);
		let embed = new Discord.MessageEmbed({
			color: 65280
		});
		if (userTerm === '.*') {
			let totalTerms = [];
			for (let abbr of abbrData) {
				totalTerms.push(abbr.title.Abbreviation);
			}
			embed.addField("List of abbreviations", `Here are all configured abbreviations. For a complete list, see <https://help.gamepedia.com/Abbreviations> or <https://c.fandom.com/Help:Abbreviations>.\n\n ${totalTerms.join(", ")}`)
				.setTitle("Full list 																	:)");

			await msg.channel.send(embed);
		}
		else if (!abbrData.length) {
			abbrData = await getAbbr(".*");
			let totalTerms = [];
			for (let abbr in abbrData) {
				totalTerms.push(abbrData[abbr].title.Abbreviation);
			}
			embed.addField("List of abbreviations", `I was unable to find your abbreviation. Here are all configured abbreviations. For a complete list, see <https://help.gamepedia.com/Abbreviations> or <https://c.fandom.com/Help:Abbreviations>.\n\n ${totalTerms.join(", ")}`)
				.setTitle("Full list 																	:)");

			await msg.channel.send(embed);
		}
		else {
			let definition = abbrData[0].title.Definition.replace(/\[\[(?:(.*?)\|)?(.*?)\]\]/g, function(_, m1, m2) {
				return `[${m2}](https://help.gamepedia.com/${(m1||m2).replace(/\s/g, '_')})`;
			});

			embed.addField("Abbreviation", abbrData[0].title.Abbreviation, true)
				.addField("Full", abbrData[0].title.Full, true)
				.addField("Definition", definition)
				.setTitle(abbrData[0].title.Abbreviation);

		  await msg.channel.send(embed);
		}
		return;
    }
	if (approvedChannels.indexOf(msg.channel.id) == -1) return;
	if (msg.content.startsWith('!languageNum')) {
		let embed = new Discord.MessageEmbed({
			color: 54741,
			title: "Language population",
		});
		await msg.guild.members.fetch();
		for (r of languageRoles) {
			let role = msg.guild.roles.cache.find(ro => ro.name == r.name);
			embed.addField(r.name,role.members.size,true);
		}
		try {
			await msg.channel.send("", {embed: embed});
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.match(/https?:\/\/.*/) && !msg.content.match(/(wikia|fandom|gamepedia)/) && msg.channel.id == channels.verification) {
		try {
			if (msg.member) {
				if (msg.member.roles.cache.find(r => moderatorRoles.indexOf(r.name) !== -1)) return;
			}
			await msg.reply("please refrain from posting external links in this channel.");
			await msg.delete();
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.toLowerCase() === '!help' && msg.channel.id == channels.bot_commands) {
		let embed = new Discord.MessageEmbed({
			color: 54741,
			title: "Available commands",
		});
		embed.addField("`!abbr`","Define abbreviations used on this server",true);
		for (g of genderRoles) {
			let gCode = '`!'+g.codes[0]+'`';
			embed.addField(gCode,g.name,true);
		}
		embed.addField("`!lang`","Show available language roles",true);
		embed.addField("`!sc`","Show options for contacting staff",true);
		embed.addField("`!ucp`","Provide informative links about UCP",true);
		try {
			await msg.channel.send("", {embed: embed});
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.toLowerCase() === '!gender' && msg.channel.id === channels.roles) {
		let embed = new Discord.MessageEmbed({
			color: 54741,
			title: "Available pronoun roles",
		});
		for (g of eGenderRoles) embed.addField(g.emoji,g.name,true);
		try {
			let reactMsg = await msg.channel.send("", {embed: embed});
			for (g of eGenderRoles) await reactMsg.react(g.emoji);
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.toLowerCase() === '!verticals' && msg.channel.id === channels.roles) {
		let embed = new Discord.MessageEmbed({
			color: 54741,
			title: "Available vertical roles",
		});
		for (g of eVerticalRoles) embed.addField(g.emoji,g.name,true);
		try {
			let reactMsg = await msg.channel.send("", {embed: embed});
			for (g of eVerticalRoles) await reactMsg.react(g.emoji);
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.toLowerCase() === '!tz' && msg.channel.id === channels.roles) {
		let embed = new Discord.MessageEmbed({
			color: 54741,
			title: "Please select the region with the time zone you most closely follow:"
		});
		for (g of eTZRoles) embed.addField(g.emoji,g.name,true);
		try {
			let reactMsg = await msg.channel.send("", {embed: embed});
			for (g of eTZRoles) await reactMsg.react(g.emoji);
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.toLowerCase() === '!platform' && msg.channel.id === channels.roles) {
		let embed = new Discord.MessageEmbed({
			color: 54741,
			title: "Please select the platform to see its channels:"
		});
		for (g of ePRoles) embed.addField(g.field,g.name,true);
		try {
			let reactMsg = await msg.channel.send("", {embed:embed});
			for (g of ePRoles) await reactMsg.react(g.emoji);
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	if (msg.content.toLowerCase().startsWith('!lang') && (msg.channel.id == channels.bot_commands || msg.channel.id == channels.roles)) {
		let embed = new Discord.MessageEmbed({
			color: 54741,
			title: "Available language roles",
		});
		if (msg.channel.id === channels.bot_commands) {
			for (r of languageRoles) embed.addField(`\`!${r.codes[0]}\``,r.name,true);
		}
		else {
			for (r of eLanguageRoles) embed.addField(r.emoji,r.name,true);
		}
		try {
			let reactMsg = await msg.channel.send("", {embed: embed});
			if (msg.channel.id === channels.roles) {
				for (g of eLanguageRoles) await reactMsg.react(g.emoji);
			}
		}
		catch (error) {
			console.log(error);
		}
		return;
	}
	let gRole = genderRoles.find(role => role.codes.indexOf(msg.content.toLowerCase().slice(1)) !== -1);
	if (msg.content.startsWith('!') && gRole !== undefined && msg.channel.id == channels.bot_commands) {
		let role = msg.guild.roles.cache.find(r => r.name == gRole.name);
		try {
			await toggleRole(msg, role);
		}
		catch (error) { console.log(error); }
		return;
	}
	let lRole = languageRoles.find(role => role.codes.indexOf(msg.content.toLowerCase().slice(1)) !== -1);
	if (msg.content.startsWith('!') && lRole !== undefined && msg.channel.id == channels.bot_commands) {
		let role = msg.guild.roles.cache.find(r => r.name == lRole.name);
		try {
			await toggleRole(msg, role);
		}
		catch (error) { console.log(error); }
		return;
	}
	if (vHelpMatch = msg.content.match(/^!verifyhelp (.*)/)) {
		user = vHelpMatch[1].replace(/(<(@.*)>|@everyone)/g, "");
		await msg.delete();
		await msg.channel.send(`Please copy and paste the following command: \`!verify ${user}\``);
		return;
	}
	if (msg.content.trim() == `<#${channels.verification}>`) {
		if (msg.member) {
			if (msg.member.roles.length === 0) {
				await msg.reply("Usage: `!verify <wiki_username>`");
				return;
			}
		}
	}
	if (msg.content.toLowerCase()=="!verify" || msg.content.toLowerCase()=="! verify") {
		await msg.channel.send("Usage: `!verify <wiki_username>`");
		return;
	}
	if (verifyMatch = msg.content.match(/^! ?verify (.*)/i)) {
		platform = "fandom";
		user = verifyMatch[1];
		if (msg.mentions.users.first()) user = msg.mentions.users.first().username;
		user = user.replace(/https:\/\/.*?\.(?:com|org)\/.*?([^:\/]*?)$/g, "$1")
					.replace(/(\||\/)/g, "")
					.replace(/#\d+/, "")
					.replace(/[<>]/g,"")
					.replace(/\s*\[.*?\]\s*/g,"")
					.replace(/#/g, "") 
					.replace(/^wiki_/i,"");
		if (user == "username") {
			await msg.reply("please provide your actual wiki username there.");
			return;
		}
		if (waitingForModerator.find(i => i.id === msg.author.id)) {
			await msg.reply("please WAIT for a moderator's assistance.");
			return;
		}
		user = capitalize(user);
		let userData = {
			name: user,
			groups: [],
			imgroups: [],
			platforms: {
				fandom: {
					id: -1,
					edits: -1,
					discordHandle: undefined,
					blocks: undefined,
					requirementsPassed: { response: false, reasons: ["requirements not loaded"]}
				},
				gamepedia: {
					id: -1,
					edits: -1,
					discordHandle: undefined,
					blocks: undefined,
					requirementsPassed: { response: false, reasons: ["requirements not loaded"]}
				}
			}
		};
		await verifyUser(userData, msg);
		return;
	}
});

client.on('guildMemberAdd', async member => {
	try {
		let channel = await member.createDM();
		if (!channel) return;
		let embed = new Discord.MessageEmbed({
			color:54741,
			title:"Welcome to Fandom/Gamepedia!",
			description:`Hey ${member}, welcome to **Fandom/Gamepedia** 🎉🤗 ! Please add your Discord tag (\`${member.user.tag}\`) to your wiki profile in the dedicated field and then use the \`!verify <wiki_username>\` bot command in <#${channels.verification}> to get access to other channels. Please read the following guides to help you add your Discord tag to your profile: [Fandom](https://community.fandom.com/wiki/Help:Profiles/Discord) or [Gamepedia](https://help.gamepedia.com/Discord). Also, familiarize yourself with <#${channels.rules}>.`       
		});
		await channel.send("",{embed:embed});
	}
	catch (error) {
		if (error.message) {
			if (!error.message.includes('Cannot send messages to this user')) console.log(error);
		}
		else console.log(error);
	}
});

client.on('guildMemberUpdate', async (oldMember, newMember) => {
	if (oldMember.nickname == newMember.nickname) return;
	if ((vIndex = verifyRename.indexOf(newMember.nickname)) > -1 && newMember.nickname != undefined) {
		verifyRename.splice(vIndex,1);
		return;
	}
	let channel = newMember.guild.channels.cache.get(channels.rename_log);
	if (!channel) return;
	try {
		let newNick = newMember.nickname;
		if (newNick == undefined) newNick = newMember.user.username;

		let oldNick = oldMember.nickname;
		if (oldNick == undefined) oldNick = oldMember.user.username;

		if ((ncri = nameChangeTimeouts.findIndex(r => r.data.oldName === newNick)) > -1) { // if member reverted their nickname to previous
			clearTimeout(nameChangeTimeouts[ncri].timeout);
			nameChangeTimeouts.splice(ncri,1);
			await channel.send(`<@${oldMember.user.id}>'s nickname was restored to ${newNick} from ${oldNick}`);
			return;
		}
		if ((ncri = nameChangeTimeouts.findIndex(r => r.data.newName === oldNick)) > -1) { // if member on the list is changing to a new name, keep track of the original
			oldNick = nameChangeTimeouts[ncri].data.oldName;
			clearTimeout(nameChangeTimeouts[ncri].timeout);
			nameChangeTimeouts.splice(ncri,1);
		}
		var similarity = stringSimilarity.compareTwoStrings(oldNick,newNick);
		if (similarity >= 0.5) {
			await channel.send(`<@${oldMember.user.id}> was renamed from ${oldNick} to ${newNick}\nSimilarity: ${similarity}`);
			return;
		}
		let embed = new Discord.MessageEmbed({
			color: 16776960,
			title: "User renamed, similarity < 0.5",
			description: `<@${oldMember.user.id}> was renamed from ${oldNick} to ${newNick}\nSimilarity: ${similarity}\nDisallow to prompt the user to revert the change (and set a 24 hour timer to reset).\nIgnore to do nothing`,
			fields: [
				{name: "Disallow", value:"⛔", inline:true},
				{name: "Ignore", value:"🆗", inline:true}
			]
		});
		let msg = await channel.send("",embed);
		await msg.react("⛔");
		await msg.react("🆗");
		if (nameChangeReactions.length > 10) {
			let oldMsg = await channel.messages.fetch(nameChangeReactions[0]);
			await removeNameChangeReactionMessage(oldMsg);
		}
		nameChangeReactions.push({id:msg.id,user:oldMember.user.id,oldName:oldNick,newName:newNick});
	}
	catch (error) {
		console.log(error);
	}
});

/*
client.on('raw', async event => {
	if (!events.hasOwnProperty(event.t)) return;
	const { d: data } = event;
	const user = client.users.cache.get(data.user_id);
	const channel = client.channels.cache.get(data.channel_id) || await user.createDM();
	if (channel.messages.cache.has(data.message_id) && event.t == 'MESSAGE_REACTION_ADD') return;
	const message = await channel.messages.fetch(data.message_id);
	const emojiKey = data.emoji.id || data.emoji.name;
	let reaction = message.reactions.cache.get(emojiKey);

	if (!reaction) {
		// Create an object that can be passed through the event like normal
		const emoji = new Discord.Emoji(client.guilds.cache.get(data.guild_id), data.emoji);
		try {
			reaction = new Discord.MessageReaction(message, emoji, 1, data.user_id === client.user.id);
		}
		catch (error) {
			if (error.message) {
				if (!error.message.startsWith('Cannot read property'))
					console.log(error);
			}
			else console.log(error);
		}
	}

	client.emit(events[event.t], reaction, user);
});*/

client.on('messageReactionAdd', async (reaction, user) => {
	if (user.id === client.user.id) return;
	if (reaction.message.partial) {
		try {
			await reaction.message.fetch();
		}
		catch (error) {
			console.error('Something went wrong when fetching the message: ', error);
			return;
		}
	}
	let msg = reaction.message;
	if (msg.author.id !== client.user.id) return;
	if ((ncrIndex = nameChangeReactions.findIndex(m => m.id === msg.id)) !== -1) {
		try {
			if (!(reaction.emoji.name == "⛔" || reaction.emoji.name == "🆗")) return;
			if (reaction.emoji.name == "⛔") {
				let channel = msg.guild.channels.cache.get(channels.general);
				let ncd = nameChangeReactions[ncrIndex];
				await channel.send(`<@${ncd.user}>, please make sure your Discord nickname resembles your wiki username.`);
				nameChangeTimeouts.push({timeout:setTimeout(async function() {
					try {
						let member = await msg.guild.members.fetch(ncd.user);
						if (member.nickname === ncd.oldName) return;
						verifyRename.push(ncd.oldName);
						await member.setNickname(ncd.oldName,"Reverting nickname");
					}
					catch (error) {
						console.log(error);
					}
				},86400000,msg,ncd),time:(new Date()).getTime(),data:ncd});
			}
			await removeNameChangeReactionMessage(msg);
			return;
		}
		catch (error) { console.log(error); }
	}
	try {
		if (reaction.emoji.name == "❌") {
			if (msg.embeds != null) {
				if (msg.embeds[0] != null) {
					if (msg.embeds[0].footer != null) {
						if (msg.embeds[0].footer.text == `Quoted by ${user.tag}`) {
							try {
								await msg.delete();
							}	
							catch (error) {
								console.log(error);
							}
						}
					}
				}
			}
			return;
		}
		let index = Object.values(roleReactionMessages).indexOf(msg.id);
		if (index === -1) return;
		let member = await findGuildMember(msg.guild, user);
		var rid;
		var role;
		if (!member) return;
		switch (Object.keys(roleReactionMessages)[index]) {
			case "gender":
				rid = eGenderRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.add(role, "By request");
			break;
			case "vertical":
				rid = eVerticalRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.add(role, "By request");
			break;
			case "language":
				rid = eLanguageRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.add(role, "By request");
			break;
			case "timezone":
				rid = eTZRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.add(role, "By request");
			break;
			case "platform":
				rid = ePRoles.find(r => r.emoji == reaction.emoji.id);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.add(role, "By request");
			break;
		}
	}
	catch (error) {
		console.log(error);
	}
});

client.on('messageReactionRemove', async (reaction, user) => {
	if (user.id === client.user.id) return;
	if (reaction.message.partial) {
		try {
			await reaction.message.fetch();
		}
		catch (error) {
			console.error('Something went wrong when fetching the message: ', error);
			return;
		}
	}
	let msg = reaction.message;
	if (msg.author.id !== client.user.id) return;
	let index = Object.values(roleReactionMessages).indexOf(msg.id);
	if (index === -1) return;
	try {
		let member = await findGuildMember(msg.guild, user);
		var rid;
		var role;
		if (!member) return;
		switch (Object.keys(roleReactionMessages)[index]) {
			case "gender":
				rid = eGenderRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.remove(role, "By request");
			break;
			case "vertical":
				rid = eVerticalRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.remove(role, "By request");
			break;
			case "language":
				rid = eLanguageRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.remove(role, "By request");
			break;
			case "timezone":
				rid = eTZRoles.find(r => r.emoji == reaction.emoji.name);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.remove(role, "By request");
			break;
			case "platform":
				rid = ePRoles.find(r => r.emoji == reaction.emoji.id);
				if (!rid) return;
				role = msg.guild.roles.cache.find(r => r.name === rid.name);
				if (!role) return;
				await member.roles.remove(role, "By request");
			break;
		}
	}
	catch (error) {
		console.log(error);
	}
});

client.on('channelUpdate', async(oldChannel, newChannel) => {
	let oldPos = oldChannel.position;
	let indCheck = affectedChannels.findIndex(ch => ch.id === newChannel.id);
	if (indCheck !== -1) {
		oldPos = affectedChannels[indCheck].origPos;
		if (oldPos === newChannel.position) affectedChannels[indCheck].status += ", and then moved back";
	}
	if (oldPos !== newChannel.position) {
		let status = "";
		if (oldPos > newChannel.position) status = `Moved up to position ${newChannel.position}`;
		if (oldPos < newChannel.position) status = `Moved down to position ${newChannel.position}`;
		let prefix = "#";
		if (newChannel.type === "category") prefix = "";
		if (status !== "") affectedChannels.push({name:prefix+newChannel.name,value:status,id:newChannel.id,parent:newChannel.parentID,origPos:oldChannel.position});		
		if (!alertQueuedForThisChannelMove) {
			setTimeout(async function() {
				let chanMoveFields = affectedChannels.filter(ch => ch.parent == null || !affectedChannels.find(c => c.id === ch.parent));
				let movedDown = chanMoveFields.filter(ch => ch.value.match(/Moved down/));
				let movedUp = chanMoveFields.filter(ch => ch.value.match(/Moved up/));
				if (movedUp.length != 0 && movedDown.length != 0) {
					if (movedUp.length > movedDown.length) chanMoveFields = movedDown;
					if (movedUp.length < movedDown.length) chanMoveFields = movedUp;
				}
				let embed = new Discord.MessageEmbed({
					color: 16776960,
					title: "Channels were moved",
					description: "Someone's screwing with channel positions again.\nMoved channel(s):",
					fields: chanMoveFields
				});
				if (chanMoveFields.length > 0) await oldChannel.guild.channels.cache.get(channels.channel_moves).send("",embed);
				alertQueuedForThisChannelMove = false;
				affectedChannels = [];
			}, 5000);
			alertQueuedForThisChannelMove = true;
		}
	}
});


client.login(config.tokens.discord);